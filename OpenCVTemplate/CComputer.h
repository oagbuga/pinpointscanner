#pragma once

namespace CTest
{
	class CComputer
	{
	public:
		CComputer();
		~CComputer();

		//template<class T>
		//T multiply(T a, T b);

		//Template functions must be implemented in headers
		template<class T>
		T multiply(T a, T b)
		{
			return a * b;
		}

		template<class T>
		T add(T a, T b)
		{
			return a + b;
		}


		//int add(int a, int b);
	};
}
