#include <opencv2/core/core.hpp>
//#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/gpu.hpp>
#include <iostream>
#include "opencv2/opencv.hpp"

#include "CComputer.h"
//#include "CComputer.cpp"

#include "Windows.h"
#include "Winuser.h"

#include <memory>

using namespace cv;
using namespace std;
using namespace CTest;

//template<class T1>
//T1 add(T1 a, T1 b)
//{
//	return a + b;
//}

struct testStruct{ int a = 2; };

class Printer
{//Hi
public:
	Printer(string name){ cout << "Printer created!"<<endl; cout<< "Printer Name:" << name<<endl; }
	~Printer(){ cout << "Printer destroyed!"<<endl; }
	void print()
	{ 
		testStruct* myStruct = new testStruct();
		myStruct->a = 5;

		int b = 10;
		double a = static_cast<double>(b) / 2;

		cout << "Printing started!"<<endl; 
		cout << "Number:" << std::to_string(a)<<endl;
		cout << "end of test" << endl;
	}
};

void test()
{
	//Alternative-1 one for the smart shared pointer creation
	//Needs two steps. 1) Printer100 is created 2) myPrinter is created
	//shared_ptr<Printer> myPrinter(new Printer("Printer100"));
	
	//Alternative-2 one for the smart shared pointer creation (faster and safer)
	//make_shared combines two steps from Alternative-1 under one step.
	//The way from Alternative-1 is not exception safe but this way is exception safe.
	shared_ptr<Printer> myPrinter = make_shared<Printer>("Printer100");

	myPrinter->print();
}


int main()
{
	//Call template function
	CTest::CComputer* myComputer = new CTest::CComputer();
	int a = myComputer->multiply(10, 20);

	//Call member function
	//TEST TEST 

	cout << "Result Addition:";
	cout << myComputer->add(1, 2);
	cout << "\n";
	cout << "Result Multiplication:";
	cout << a;
	cout << "This is only for test";

	//CComputer* myComputer=new CComputer();
	//int a = myComputer->multiply(1, 2);
	
	//int a = CComputer::add(1, 2);

	//CComputer* myComputer;
	//myComputer = new CComputer();

	//int a = myComputer->add(1, 2);

	//if (argc != 2)
	//{
	//	cout << " Usage: display_image ImageToLoadAndDisplay" << endl;
	//	return -1;
	//}

	//Mat image;
    //image = imread(argv[1], CV_LOAD_IMAGE_COLOR);   // Read the file
	
	Mat image1 = imread("D:\\PinPoint3D\\Software\\PinPointScanner\\PhaseImages\\Img1.tif",CV_LOAD_IMAGE_GRAYSCALE);
	Mat image2 = imread("D:\\PinPoint3D\\Software\\PinPointScanner\\PhaseImages\\Img2.tif", CV_LOAD_IMAGE_GRAYSCALE);
	Mat image3 = imread("D:\\PinPoint3D\\Software\\PinPointScanner\\PhaseImages\\Img3.tif", CV_LOAD_IMAGE_GRAYSCALE);

	if (!image1.data)                              // Check for invalid input
	{
		cout << "Could not open or find the image" << std::endl;
		return -1;
	}

	//Implement the phase wrapping
	


	//Display
	namedWindow("Display window", WINDOW_AUTOSIZE);// Create a window for display.
	
	for (int s = 1; s < 100; s++)
	{
		int d = s % 3;
		//string dStr = std::to_string(d);

		//MessageBox(NULL, dStr.c_str, NULL, MB_OK);

		int index1 = d;
		std::string test1 = std::to_string(index1);
		//MessageBoxA(NULL, test1.c_str(), "testx", MB_OK);

		if (d == 1)
		{
			imshow("Display window", image1);
			//MessageBox(NULL, L"Image-1",NULL, NULL);
			//MessageBox(NULL, L"Image-1", NULL, MB_OK);
		}
		else if (d == 2)
		{
			imshow("Display window", image2);
		}
		else if (d == 0)
		{
			cv::addWeighted(image1, 0, image2, 0, 0, image3);

			imshow("Display window", image3);
		}

		//Sleep(1000);
		//cout << std::to_string(d) << std::endl;
		
		cv::waitKey(1000);
		
		//_sleep(500);
		//imshow("Display window", image);                   // Show our image inside it.
	}

	waitKey(0);                                          // Wait for a keystroke in the window
	return 0;
}



