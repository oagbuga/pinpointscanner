﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppGUI
{
    /// <summary>
    /// Interaction logic for UC_AdminWindow.xaml
    /// </summary>
    public partial class UC_AdminWindow : UserControl
    {

        public delegate void EventHandler_AdminWindow(Object sender, RoutedEventArgs e); //Delegate
        public event EventHandler_AdminWindow ProcessDone_GoSampleWindow; //This event will be executed when the process will be done.
        //public event EventHandler_StartWindow ProcessDone_GoAutomaticWindow; //This event will be executed when the process will be done.
        //public event EventHandler_StartWindow ProcessDone_GoSetupWindow; //This event will be executed when the process will be done.
        //public event EventHandler_StartWindow ProcessDone_GoPasswordWindow; //This event will be executed when the process will be done.


        public UC_AdminWindow()
        {
            InitializeComponent();
        }

        private void mBtnSampleType_Click(object sender, RoutedEventArgs e)
        {
            ProcessDone_GoSampleWindow(this, new RoutedEventArgs()); //Execute the DivisionDone event
        }

        private void mBtnSamplePlus_Click(object sender, RoutedEventArgs e)
        {

            int newNumber = Convert.ToInt16(mLabelAmountNrText.Content)+1;
            if (newNumber <= 10)
            {
                mLabelAmountNrText.Content = Convert.ToString(newNumber);
            }
            if(newNumber==10)
            {
                //mLabelAmountNrText.Background = Brushes.Red;
            }
        }

        private void mBtnSampleMinus_Click(object sender, RoutedEventArgs e)
        {
            int newNumber = Convert.ToInt16(mLabelAmountNrText.Content) - 1;
            if (newNumber >= 1)
            {
                mLabelAmountNrText.Content = Convert.ToString(newNumber);
            }
            if (newNumber == 1)
            {
                //mLabelAmountNrText.Background = Brushes.Red;
            }
        }

        private void UserControl_MouseMove(object sender, MouseEventArgs e)
        {
            //mBtnSampleType.Content = UC_SampleType.sSelectedType;
        }
    }
}
