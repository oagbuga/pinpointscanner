﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppGUI
{
    /// <summary>
    /// Interaction logic for UC_SampleType.xaml
    /// </summary>
    public partial class UC_SampleType : UserControl
    {
        public delegate void EventHandler_SampleWindow(Object sender, RoutedEventArgs e); //Delegate
        public event EventHandler_SampleWindow ProcessDone_GoAdminWindow; //This event will be executed when the process will be done.

        public static string sSelectedType = "Sampletype";

        public UC_SampleType()
        {
            InitializeComponent();
        }

        private void mBtnSampleType1_Click(object sender, RoutedEventArgs e)
        {
            ProcessDone_GoAdminWindow(this, new RoutedEventArgs()); //Execute the DivisionDone event

            sSelectedType = "Liver";
        }
    }
}
