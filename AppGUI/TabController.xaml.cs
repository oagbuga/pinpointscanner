﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppGUI
{
    /// <summary>
    /// Interaction logic for TabController.xaml
    /// </summary>
    public partial class TabController : UserControl
    {
        public delegate void EventHandler_TabCtrlWindow(Object sender, RoutedEventArgs e); //Delegate

        public TabController()
        {
            InitializeComponent();
        }

        private void btnTab1_Click(object sender, RoutedEventArgs e)
        {
            btnTab1.Style = (Style)FindResource("resourceBtnTabType1B");
            btnTab2.Style = (Style)FindResource("resourceBtnTabType2A");
            btnTab3.Style = (Style)FindResource("resourceBtnTabType2A");
            btnTab4.Style = (Style)FindResource("resourceBtnTabType3A");

            
        }

        private void btnTab2_Click(object sender, RoutedEventArgs e)
        {
            btnTab1.Style = (Style)FindResource("resourceBtnTabType1A");
            btnTab2.Style = (Style)FindResource("resourceBtnTabType2B");
            btnTab3.Style = (Style)FindResource("resourceBtnTabType2A");
            btnTab4.Style = (Style)FindResource("resourceBtnTabType3A");

        }

        private void btnTab3_Click(object sender, RoutedEventArgs e)
        {
            btnTab1.Style = (Style)FindResource("resourceBtnTabType1A");
            btnTab2.Style = (Style)FindResource("resourceBtnTabType2A");
            btnTab3.Style = (Style)FindResource("resourceBtnTabType2B");
            btnTab4.Style = (Style)FindResource("resourceBtnTabType3A");
        }

        private void btnTab4_Click(object sender, RoutedEventArgs e)
        {
            btnTab1.Style = (Style)FindResource("resourceBtnTabType1A");
            btnTab2.Style = (Style)FindResource("resourceBtnTabType2A");
            btnTab3.Style = (Style)FindResource("resourceBtnTabType2A");
            btnTab4.Style = (Style)FindResource("resourceBtnTabType3B");
        }

        private void btnTab_Robovis(object sender, RoutedEventArgs e)
        {
            //This part is temporarily implemented for exiting the gui...
            //Stable version...
            Environment.Exit(0);
        }
    }
}
