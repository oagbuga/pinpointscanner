﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//Roche
using System.Runtime.InteropServices;
using System.Windows.Interop;

using Leap;
using System.Threading;

namespace AppGUI
{
   

    public partial class SampleListener
    {
        public void OnInit(Controller controller)
        {
            //Console.WriteLine("Initialized");
            MessageBox.Show("Initialized");
        }

        public void OnConnect(object sender, DeviceEventArgs args)
        {
            //Console.WriteLine("Connected");
            MessageBox.Show("Connected");
        }

        public void OnDisconnect(object sender, DeviceEventArgs args)
        {
            //Console.WriteLine("Disconnected");
            MessageBox.Show("Disconnected");
        }


        public void OnFrame(object sender, FrameEventArgs args)
        {
            // Get the most recent frame and report some basic information
            Leap.Frame frame = args.frame;

            //Console.WriteLine(
            //  "Frame id: {0}, timestamp: {1}, hands: {2}",
            //  frame.Id, frame.Timestamp, frame.Hands.Count
            //);

            //MessageBox.Show("Frame id:" + frame.Id + "timestamp:" + frame.Timestamp + "hands:" + frame.Hands.Count);


            foreach (Hand hand in frame.Hands)
            {
                //Console.WriteLine("  Hand id: {0}, palm position: {1}, fingers: {2}",
                //  hand.Id, hand.PalmPosition, hand.Fingers.Count);
                
                // Get the hand's normal vector and direction
                Leap.Vector normal = hand.PalmNormal;
                Leap.Vector direction = hand.Direction;

                // Calculate the hand's pitch, roll, and yaw angles
                //Console.WriteLine(
                //  "  Hand pitch: {0} degrees, roll: {1} degrees, yaw: {2} degrees",
                //  direction.Pitch * 180.0f / (float)Math.PI,
                //  normal.Roll * 180.0f / (float)Math.PI,
                //  direction.Yaw * 180.0f / (float)Math.PI
                //);

                

                // Get the Arm bone
                Arm arm = hand.Arm;
                //Console.WriteLine(
                //  "  Arm direction: {0}, wrist position: {1}, elbow position: {2}",
                //  arm.Direction, arm.WristPosition, arm.ElbowPosition
                //);

                // Get fingers
                foreach (Finger finger in hand.Fingers)
                {
                    if (finger.Type == Finger.FingerType.TYPE_INDEX)
                    {
                        //Console.WriteLine(
                        //  "    Finger id: {0}, {1}, length: {2}mm, width: {3}mm",
                        //  finger.Id,
                        //  finger.Type.ToString(),
                        //  finger.Length,
                        //  finger.Width
                        //);

                        //MessageBox.Show("Finger id:" + finger.Id);

                        //Mouse.SetCursor.


                        // Get finger bones
                        Bone bone;
                        for (int b = 0; b < 4; b++)
                        {
                            bone = finger.Bone((Bone.BoneType)b);
                            //Console.WriteLine(
                            //  "      Bone: {0}, start: {1}, end: {2}, direction: {3}",
                            //  bone.Type, bone.PrevJoint, bone.NextJoint, bone.Direction
                            //);
                        }

                        // Move the cursor
                        //MouseCursor.MoveCursor(20, 20);


                        // Get the closest screen intercepting a ray projecting from the finger

                        //if (finger.Type == Finger.FingerType.TYPE_PINKY)
                        //{
                        //Leap.Vector currentPosition = finger.TipPosition;
                        Leap.Vector currentPosition = finger.StabilizedTipPosition;

                        //int offX = 18;
                        //int offY = 4;
                        //MouseCursor.MoveCursor(Convert.ToInt16(currentPosition[0]) * offX, Convert.ToInt16(currentPosition[1]) * offY);

                        int appWidth = 3000;
                        int appHeight = 2000;

                        InteractionBox iBox = frame.InteractionBox;// leap.Frame().InteractionBox;
                                                                   //if (frame.Hands.Count > 0)
                                                                   //{
                                                                   //    hand = leap.Frame().Hands[0];
                                                                   //    Finger finger = hand.Fingers[1];

                        Leap.Vector leapPoint = finger.StabilizedTipPosition;
                        Leap.Vector normalizedPoint = iBox.NormalizePoint(leapPoint, false);

                        float appX = normalizedPoint.x * appWidth;
                        float appY = (1 - normalizedPoint.y) * appHeight;

                        MouseCursor.MoveCursor(Convert.ToInt16(appX), Convert.ToInt16(appY));

                        //The z-coordinate is not used
                        //}


                        //}

                        //if(hand.GrabAngle>3.5)
                        //if (finger.Direction.Pitch > 3.5)
                        if (finger.TipVelocity.z > 50)
                        {
                            leapPoint = finger.StabilizedTipPosition;
                            normalizedPoint = iBox.NormalizePoint(leapPoint, false);

                            appX = normalizedPoint.x * appWidth;
                            appY = (1 - normalizedPoint.y) * appHeight;

                            //MouseClick.MouseLeft(MOUSEEVENTF_LEFTUP, 0, 0, 0, new System.IntPtr());
                            const int MOUSEEVENTF_LEFTDOWN = 0x0002;
                            const int MOUSEEVENTF_LEFTUP = 0x0004;

                            //MouseClick.MouseLeft(MOUSEEVENTF_LEFTDOWN, Convert.ToUInt16(appX), Convert.ToUInt16(appY), 0, 0);
                            //MouseClick.MouseLeft(MOUSEEVENTF_LEFTUP, Convert.ToUInt16(appX), Convert.ToUInt16(appY), 0, 0);

                            MouseClick.mouse_event(MOUSEEVENTF_LEFTDOWN, Convert.ToInt16(appX), Convert.ToInt16(appY), 0, 0);
                            System.Threading.Thread.Sleep(10);
                            MouseClick.mouse_event(MOUSEEVENTF_LEFTUP, Convert.ToInt16(appX), Convert.ToInt16(appY), 0, 0);
                        }
                    }
                }
            }

            if (frame.Hands.Count != 0)
            {
                //Console.WriteLine("");
            }
        }



        public void OnServiceConnect(object sender, ConnectionEventArgs args)
        {
            //Console.WriteLine("Service Connected");
            MessageBox.Show("Service Connected");
        }

        public void OnServiceDisconnect(object sender, ConnectionLostEventArgs args)
        {
            //Console.WriteLine("Service Disconnected");
            MessageBox.Show("Service Disconnected");
        }

        public void OnServiceChange(Controller controller)
        {
            //Console.WriteLine("Service Changed");
            MessageBox.Show("Service Changed");
        }

        public void OnDeviceFailure(object sender, DeviceFailureEventArgs args)
        {
            //Console.WriteLine("Device Error");
            //Console.WriteLine("  PNP ID:" + args.DeviceSerialNumber);
            //Console.WriteLine("  Failure message:" + args.ErrorMessage);

            MessageBox.Show("Device Error" + " PNP ID:" + args.DeviceSerialNumber + " Message:" + args.ErrorMessage);
        }

        public void OnLogMessage(object sender, LogEventArgs args)
        {
            switch (args.severity)
            {
                case Leap.MessageSeverity.MESSAGE_CRITICAL:
                    //Console.WriteLine("[Critical]");
                    break;
                case Leap.MessageSeverity.MESSAGE_WARNING:
                    //Console.WriteLine("[Warning]");
                    break;
                case Leap.MessageSeverity.MESSAGE_INFORMATION:
                    //Console.WriteLine("[Info]");
                    break;
                case Leap.MessageSeverity.MESSAGE_UNKNOWN:
                    //Console.WriteLine("[Unknown]");
                    break;
            }
            //Console.WriteLine("[{0}] {1}", args.timestamp, args.message);
        }
    }

    class MouseCursor
    {
        [DllImport("user32.dll")]
        private static extern bool SetCursorPos(int x, int y);

        public static void MoveCursor(int x, int y)
        {
            SetCursorPos(x, y);
        }

    }

    class MouseClick
    {
        //[DllImport("user32.dll")]
        //static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint dwData,
        //   UIntPtr dwExtraInfo);

        //public static void MouseLeft(uint dwFlags, uint dx, uint dy, uint dwData,
        //   UIntPtr dwExtraInfo)
        //{
        //    mouse_event(dwFlags, dx, dy, dwData, dwExtraInfo);
        //}

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        public const int MOUSEEVENTF_LEFTDOWN = 0x02;
        public const int MOUSEEVENTF_LEFTUP = 0x04;

        //This simulates a left mouse click
        public static void LeftMouseClick(int xpos, int ypos)
        {
            MouseCursor.MoveCursor(xpos, ypos);
            mouse_event(MOUSEEVENTF_LEFTDOWN, xpos, ypos, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTUP, xpos, ypos, 0, 0);
        }
    }

    //Call like this
    //MouseLeft(MOUSEEVENTF_LEFTUP, 0, 0, 0, new System.IntPtr());

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        UC_CameraWindow m_ucCameraWindow;
        UC_AdminWindow m_ucAdminWindow;
        UC_SampleType m_ucSampleWindow;
        UC_Block m_ucBlockWindow;
        TabController m_ucTabWindow;

        //private Controller mLeapCtrl = new Controller();

        public static float posX = 0;

        public MainWindow()
        {
            InitializeComponent();

            m_ucCameraWindow = new UC_CameraWindow();
            m_ucAdminWindow = new UC_AdminWindow();
            m_ucSampleWindow = new UC_SampleType();
            m_ucTabWindow = new TabController();
            m_ucBlockWindow = new UC_Block();


            stackPanel_HMI.Children.Add(m_ucAdminWindow);
            //stackPanel_HMI.Children.Add(m_ucBlockWindow);

            //Subscribe for Events
            m_ucAdminWindow.ProcessDone_GoSampleWindow += _ucAdminWindow_ProcessDone_GoSampleWindow;
            m_ucSampleWindow.ProcessDone_GoAdminWindow += _ucSampleWindow_ProcessDone_GoAdminWindow;


            controllerHidden_HMI.Visibility = Visibility.Hidden;

            controller_HMI.SetValue(Grid.RowProperty, 0);
            stackPanel_HMI.SetValue(Grid.RowProperty, 1);


            //Bind events for Leap
            //mLeapCtrl.StartConnection();

            // Set up our listener:
            // Set up our listener:
            SampleListener listener = new SampleListener();
            //mLeapCtrl.Connect += listener.OnServiceConnect;
            //mLeapCtrl.Disconnect += listener.OnServiceDisconnect;
            //mLeapCtrl.FrameReady += listener.OnFrame;
            //mLeapCtrl.Device += listener.OnConnect;
            //mLeapCtrl.DeviceLost += listener.OnDisconnect;
            //mLeapCtrl.DeviceFailure += listener.OnDeviceFailure;
            //mLeapCtrl.LogMessage += listener.OnLogMessage;

            //this.Cursor = new Cursor("D:\\Roche\\Metro X\\MetroAlt.ani");
            //this.Cursor = new Cursor("D:\\Roche\\Large Cursors\\bigcursr.ani");
            //m_ucAdminWindow.mPosLabel.Content = "1234";

            //m_ucCameraWindow.InitHalconWindow();
            //HObject hImage;
            //HOperatorSet.ReadImage(out hImage, "D:\\Roche\\Barcode.png");

            //HOperatorSet.DispImage(hImage, m_ucCameraWindow._halconWindowPart1);

        }

        private void _ucTabWindow_ProcessDone_GoMainWindow(object sender, RoutedEventArgs e)
        {
            stackPanel_HMI.Children.Remove(m_ucAdminWindow);
            stackPanel_HMI.Children.Add(m_ucBlockWindow);
        }

        private void _ucSampleWindow_ProcessDone_GoAdminWindow(object sender, RoutedEventArgs e)
        {
            stackPanel_HMI.Children.Remove(m_ucSampleWindow);
            stackPanel_HMI.Children.Add(m_ucAdminWindow);
        }

        private void _ucAdminWindow_ProcessDone_GoSampleWindow(object sender, RoutedEventArgs e)
        {
            stackPanel_HMI.Children.Remove(m_ucAdminWindow);
            stackPanel_HMI.Children.Add(m_ucSampleWindow);
        }

        private void TabController_Loaded(object sender, RoutedEventArgs e)
        {
            
        }


        private void MousePosition(object sender, MouseEventArgs e)
        {
            var relativePosition = e.GetPosition(this);
            var point = PointToScreen(relativePosition);

            if(point.Y<2)
            {
                controllerHidden_HMI.Visibility = Visibility.Visible;
                controller_HMI.SetValue(Grid.RowProperty, 1);
                stackPanel_HMI.SetValue(Grid.RowProperty, 2);
            }
            else
            {
                controllerHidden_HMI.Visibility = Visibility.Hidden;
                controller_HMI.SetValue(Grid.RowProperty, 0);
                stackPanel_HMI.SetValue(Grid.RowProperty, 1);
            }
        }

        [Obsolete("This is the old method!")]
        public void SetCamera()
        {

        }
    }
}
