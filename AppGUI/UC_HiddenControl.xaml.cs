﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppGUI
{
    /// <summary>
    /// Interaction logic for UC_HiddenControl.xaml
    /// </summary>
    public partial class UC_HiddenControl : UserControl
    {
        public UC_HiddenControl()
        {
            InitializeComponent();
        }

        private void ExitApplication(object sender, RoutedEventArgs e)
        {
            //Environment.Exit(0);
            Application.Current.Shutdown();
        }
    }
}
